/**
 * Polyfill forEach
 */
(function () {
  if ( typeof NodeList.prototype.forEach === "function" ) return false;
  NodeList.prototype.forEach = Array.prototype.forEach;
})();

const xhr = new XMLHttpRequest(),

/**
 * Reset Make, Models, Versions
 */
resetMake = () => {
  const marcas = document.querySelector('[name="Marcas"]'),
  modelos = document.querySelector('[name="Modelos"]'),
  versao = document.querySelector('[name="Versao"]');
  xhr.open('GET', 'api/marcas.php');
  xhr.onreadystatechange = (e) => {
    if (e.target.readyState == 4 && e.target.status == 200) {
      marcas.innerHTML = '<option value="">Todas</option>';
      modelos.innerHTML = '<option value="">Todos</option>';
      modelos.parentNode.classList.add('input--disabled');
      versao.innerHTML = '<option value="">Todas</option>';
      versao.parentNode.classList.add('input--disabled');
      JSON.parse(e.target.response).map(item => marcas.insertAdjacentHTML('beforeend', `<option value="${item.ID}">${item.Name}</option>`));
      marcas.addEventListener('change', event => resetModel(event.target.value));
    }
  };
  xhr.send();
},
resetModel = (id) => {
  const modelos = document.querySelector('[name="Modelos"]'),
  versao = document.querySelector('[name="Versao"]');
  xhr.open('GET', `api/modelos.php?id=${id}`);
  xhr.onreadystatechange = (e) => {
    if (e.target.readyState == 4 && e.target.status == 200) {
      modelos.innerHTML = '<option value="">Todos</option>';
      modelos.parentNode.classList.remove('input--disabled');
      versao.innerHTML = '<option value="">Todas</option>';
      versao.parentNode.classList.add('input--disabled');
      JSON.parse(e.target.response).map(item => modelos.insertAdjacentHTML('beforeend', `<option value="${item.ID}">${item.Name}</option>`));
      modelos.addEventListener('change', event => resetVersion(event.target.value));
    }
  };
  xhr.send();
},
resetVersion = (id) => {
  const versao = document.querySelector('[name="Versao"]');
  xhr.open('GET', `api/versao.php?id=${id}`);
  xhr.onreadystatechange = (e) => {
    if (e.target.readyState == 4 && e.target.status == 200) {
      versao.innerHTML = '<option value="">Todas</option>';
      versao.parentNode.classList.remove('input--disabled');
      JSON.parse(e.target.response).map(item => versao.insertAdjacentHTML('beforeend', `<option value="${item.ID}">${item.Name}</option>`));
    }
  };
  xhr.send();
};
resetMake();

/**
 * Changes Type Vehicle
 */
document.querySelectorAll('[name="TipoVeiculo"]').forEach(input => {
  input.addEventListener('change', event => {
    const moto = input.value==='moto' ? 1 : 0;
    document.querySelector('.search__sale-button').textContent = moto ? 'Vender minha moto' : 'Vender meu carro';
    document.querySelectorAll('[name="ConservacaoVeiculo"]').forEach(item => item.checked ? '' : item.click());
    resetMake();
  });
});

/**
 * Box Float
 */
document.addEventListener('click', (e) => {
  const _tag = e.target,
  _box = _tag.closest('.input__boxrange');
  if(_tag.hasAttribute('data-input-option') || _box){
    const _focus = document.querySelector('.input__boxrange-focus');
    if(_focus && !_box){
      _focus.classList.remove('input__boxrange-focus');
    }
    if(!_box){
      _tag.nextElementSibling.classList.add('input__boxrange-focus');
    }
  } else {
    document.querySelectorAll('.input__boxrange').forEach(input => input.classList.remove('input__boxrange-focus'));
  }
});