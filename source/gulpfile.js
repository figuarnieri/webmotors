const gulp = require('gulp'),
concat = require('gulp-concat'),
uglify = require('gulp-uglify'),
sass = require('gulp-sass'),
image = require('gulp-image'),
rename = require('gulp-rename'),
babel = require('gulp-babel');

gulp.task('css', () => {
  gulp.src(['scss/*.scss', 'scss/**/*.scss'])
  .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest('../dist/css'))
})
.task('js', () => {
  gulp.src(['js/*.js', 'js/**/*.js'])
  .pipe(babel({presets: ['env']}))
  .pipe(rename({suffix: '.min'}))
  .pipe(uglify({preserveComments: false}))
  .pipe(gulp.dest('../dist/js'))
})
.task('img', () => {
  gulp.src('img/*')
  .pipe(image({
    pngquant: true,
    optipng: true,
    zopflipng: false,
    jpegRecompress: true,
    jpegoptim: true,
    mozjpeg: true,
    gifsicle: true,
    svgo: true,
    concurrent: 7
  }))
  .pipe(gulp.dest('../dist/img'))
})
.task('watch', () => {
  gulp.watch(['scss/*.scss', 'scss/**/*.scss'], ['css'])
  gulp.watch(['js/*.js', 'js/**/*.js'], ['js'])
  gulp.watch('img/*', ['img'])
})
.task('default', ['watch']);
