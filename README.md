# Teste Front-end Webmotors

## Info
Desenvolvi o algumas partes do código baseado em PHP. Neste caso seria necessário um server apache para rodar o projeto.

## Estrutura de Pastas
Todos os códigos de Javascript e CSS estão na pasta source do Projeto.
Este projeto foi desenvolvido em Gulp, portanto caso seja necessário alterar algum código Javascript ou CSS, será necessário ter instalado Node.js na máquina, e instalar as dependencias do package.json, gulp e gulp-cli.

### api
Apenas as páginas para o retorno do Webservice

### img
Imagens do Projeto

### js
Arquivos .js

### scss
Arquivos scss, para gerar os arquivos css

#### base
Arquivos css bases para o projeto (reset, responsive e etc...)

#### componets
Components CSS