<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Carros Usados, Novos, Semi Novos e Motos - Compra e Venda - Webmotors</title>
	<meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="cache-control" content="no-cache">
  <meta http-equiv="expires" content="0">
  <meta http-equiv="pragma" content="no-cache">

  <meta property="og:locale" content="pt-br">
  <meta property="og:type" content="website">
  <meta property="og:title" content="Carros Usados, Novos, Semi Novos e Motos - Compra e Venda - Webmotors">
  <meta property="og:description" content="Webmotors é o maior e melhor site para comprar, vender, financiar, fazer seguro e saber tudo sobre veículos. Líder do segmento!">
  <meta name="twitter:title" content="Carros Usados, Novos, Semi Novos e Motos - Compra e Venda - Webmotors">
  <meta name="twitter:description" content="Webmotors é o maior e melhor site para comprar, vender, financiar, fazer seguro e saber tudo sobre veículos. Líder do segmento!">

  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2, minimum-scale=1">
  <meta name="robots" content="index, follow">
  <meta name="description" content="Webmotors é o maior e melhor site para comprar, vender, financiar, fazer seguro e saber tudo sobre veículos. Líder do segmento!">

  <link rel="icon" type="image/x-icon" href="favicon.ico">
  <link rel="stylesheet" href="dist/css/all.min.css">
  <script async src="dist/js/app.min.js"></script>
</head>
<body>
  <div class="wrap">
    <header class="header">
      <svg class="header__svg" version="1.0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 442 114.9">
          <circle class="st0" cx="55.2" cy="57.9" r="38.5"/>
          <path class="st1" d="M74.1,39.2c-1.5,0-2.8,1.3-2.8,2.8v11.5c0,0.8-0.7,1.5-1.5,1.5H59.5c-0.8,0-1.5-0.7-1.5-1.5V42 c0-1.5-1.3-2.8-2.8-2.8s-2.8,1.3-2.8,2.8v11.5c0,0.8-0.7,1.5-1.5,1.5H40.5c-0.8,0-1.5-0.7-1.5-1.5V42c0-1.5-1.3-2.8-2.8-2.8 c-1.5,0-2.8,1.3-2.8,2.8v31.7c0,1.5,1.3,2.8,2.8,2.8c1.5,0,2.8-1.3,2.8-2.8V62.2c0-0.8,0.7-1.5,1.5-1.5h10.3 c0.8,0,1.5,0.7,1.5,1.5v11.5c0,1.5,1.3,2.8,2.8,2.8s2.8-1.3,2.8-2.8V62.2c0-0.8,0.7-1.5,1.5-1.5h10.3c0.8,0,1.5,0.7,1.5,1.5 v11.5c0,1.5,1.3,2.8,2.8,2.8s2.8-1.3,2.8-2.8V42C76.9,40.5,75.6,39.2,74.1,39.2z"/>
          <path class="st2" d="M183.6,60c2.2-2.2,3.3-5.2,3.2-9.1c0-8.5-5.2-13.4-14.3-13.4c-5,0-9.1,1.5-11.8,4.3 c-2.5,2.5-3.7,5.8-3.7,9.7l0,12.6c0,8.3,6.1,13.6,15.2,13.8c0.6,0,8.2,0.4,13.7-5.3c1-1,1.1-2.7,0.2-3.9 c-0.2-0.3-0.7-0.9-1-1.2c-0.9-1.1-2.7-1.5-3.7-0.8c-0.2,0.2-3.9,3.2-8.5,3.4c-2.2,0.1-4-0.2-5.3-1.4c-1.1-1-1.7-2.5-1.7-4.2 l0-1.2c2.2,0.3,4.4,0.5,6.6,0.5C177.5,63.8,181.2,62.5,183.6,60z M165.9,56.1l0-5.1c0-1.7,0.5-3.2,1.5-4.2 c1.1-1.1,2.9-1.7,5-1.7c3.9,0,6,2.3,6,6.2c0,4.9-3.9,5.2-6,5.2C171.3,56.6,168.4,56.4,165.9,56.1z"/>
          <path class="st2" d="M209.3,37.6c-0.3,0-7.7,0-9.2,0l0-8.6c0,0,0,0,0,0v-6.4c0-1.6-1.3-3-3-3l-3,0c-1.6,0-3,1.3-3,3 l0.1,42.2c0,7.9,6.3,13.2,15.6,13.2c4.9,0,8.9-1.5,11.6-4.2c2.6-2.6,3.9-6.2,3.9-10.5l0-12.6 C222.6,45.8,220.8,37.6,209.3,37.6z M207,69.8c-4.7,0-6.7-1.7-6.7-5.5l-0.1-18.6c3.9,0,5.4,0,7.2,0c4.6,0,6.3,1.2,6.3,4.5 c0,3.2,0,13,0,13C213.7,65.7,213.7,69.8,207,69.8z"/>
          <path class="st2" d="M297.5,37.5c-5,0-9.1,1.5-11.9,4.3c-2.6,2.6-4,6.2-3.9,10.4l0,11c0,8.9,6.3,14.7,15.9,14.7 c5,0,9.1-1.5,11.9-4.3c2.6-2.6,4-6.2,4-10.3l0-11C313.5,43.3,307.2,37.5,297.5,37.5z M302.9,68.1c-1.2,1.2-3,1.8-5.3,1.8 c-4.5,0-7-2.4-7-6.6l0-11.1c0-2,0.6-3.6,1.7-4.7c1.2-1.2,3-1.8,5.3-1.8c4.5,0,7,2.3,7,6.6l0,11.1 C304.6,65.3,304.1,66.9,302.9,68.1z"/>
          <path class="st2" d="M357.9,37.5c-5,0-9.1,1.5-11.9,4.3c-2.6,2.6-4,6.2-3.9,10.4l0,11c0,8.9,6.3,14.7,15.9,14.7 c5,0,9.1-1.5,11.9-4.3c2.6-2.6,4-6.2,4-10.3l0-11C373.8,43.3,367.5,37.5,357.9,37.5z M363.3,68.1c-1.2,1.2-3,1.8-5.3,1.8 c-4.5,0-7-2.4-7-6.6l0-11.1c0-2,0.6-3.6,1.7-4.7c1.2-1.2,3-1.8,5.3-1.8c4.5,0,7,2.3,7,6.6l0,11.1 C365,65.3,364.4,66.9,363.3,68.1z"/>
          <path class="st2" d="M399.1,40.5c0-1.6-1.3-3-3-3l0,0c-1-0.1-3.8,0-3.8,0c-12.6,0.5-13.6,4.8-13.5,16.6l0,20.8 c0,1.6,1.3,3,3,3l3,0c1.6,0,3-1.3,3-3c0,0,0-20.5,0-20.5c-0.1-5.9,0.1-8.6,4.5-8.6c1,0,2.9,0,3.9,0c1.6,0,3-1.3,3-3V40.5z"/>
          <path class="st2" d="M336.1,69.9c-1,0-3.1,0-4.1,0c-3.8-0.1-4.6-4.4-4.6-7.8c0-3.6,0-14.9,0-14.9h8.4c1.6,0,3-1.3,3-3v-1.9 c0-1.6-1.3-3-3-3h-8.4l0-4.7v0v-6.4c0-1.6-1.3-3-3-3h-3c-1.6,0-3,1.3-3,3c0,0,0,24.5,0,32.7c0,9.8,2.2,16.7,13.1,16.9 c1.1,0,4.5,0,4.5,0c1.6,0,3-1.3,3-3v-2.1C339.1,71.2,337.7,69.9,336.1,69.9z"/>
          <path class="st2" d="M262.5,37.5c-0.6,0-19.9,0-19.9,0c-6.8,0-14.8,2.6-15,14.3l0.1,23.1c0,1.6,1.3,3,3,3l3,0 c1.6,0,3-1.3,3-3l0-23.4c0-1.4,0.1-3.3,1.5-4.7c0.9-0.9,2.2-1.3,3.8-1.3l3,0c0,0,2.6,0,2.8,2.8l0,26.6c0,1.6,1.3,3,3,3l3,0 c1.6,0,2.9-1.3,3-2.9c0-0.1,0-26.5,0-26.7c0-1.8,1.5-2.9,2.9-2.9l3.1,0c4.7,0,5.1,4.8,5.1,6.3l0.1,23.1c0,1.6,1.3,3,3,3l3,0 c1.6,0,3-1.3,3-3l0-23.7C276.6,42.7,271.4,37.5,262.5,37.5z"/>
          <path class="st2" d="M421.7,57.5c-5.6-4.6-14.6-6.1-11.7-10.7c0.9-1.4,2.8-1.5,4.8-1.4c1.3,0.1,2.5,0.4,3.5,0.8 c1.9,0.9,3.1,1.9,3.2,2c0.2,0.1,0.3,0.2,0.5,0.2c1,0.3,2.4-0.1,3.2-1c0.2-0.3,0.7-0.9,1-1.2c0.9-1.2,0.8-2.8-0.2-3.9 c-0.2-0.2-0.4-0.4-0.6-0.5c-0.2-0.2-1.5-1.2-1.7-1.4l0,0c-4.3-2.9-9.1-2.9-9.6-2.9c-8.3,0.2-13.5,4-13.5,11.6 c0,1.8,1.2,5.6,6.8,8.7c6.7,3.7,12.3,7,10.5,10.4c-0.7,1.4-3,2-5,1.9c-1.6-0.1-3.1-0.6-4.4-1.2c-1.5-0.8-2.3-1.5-2.8-1.8 c-1-0.6-2.7-0.2-3.5,0.9c-0.2,0.3-0.7,0.9-1,1.2c-0.7,0.9-0.8,2-0.4,3c0.1,0.3,0.3,0.6,0.6,0.9c0.7,0.8,1.5,1.4,2.3,1.9l0,0 c4.5,3.3,9.3,3,9.8,3c8.3-0.2,12.5-4.7,13-10.8C426.7,64.5,426,61,421.7,57.5z"/>
          <path class="st2" d="M117.1,77.9c0.6,0,19.9,0,19.9,0c6.8,0,14.8-2.6,15-14.3l-0.1-23.1c0-1.6-1.3-3-3-3l-3,0c-1.6,0-3,1.3-3,3 l0,23.4c0,1.4-0.1,3.3-1.5,4.7c-0.9,0.9-2.2,1.3-3.8,1.3l-3,0c0,0-2.6,0-2.8-2.8l0-26.6c0-1.6-1.3-3-3-3l-3,0 c-1.6,0-2.9,1.3-3,2.9c0,0.1,0,26.5,0,26.7c0,1.8-1.5,2.9-2.9,2.9l-3.1,0c-4.7,0-5.1-4.8-5.1-6.3l-0.1-23.1c0-1.6-1.3-3-3-3l-3,0 c-1.6,0-3,1.3-3,3l0,23.7C103,72.7,108.2,77.9,117.1,77.9z"/>
        </svg>
    </header>
    <main>
      <form class="search">
        <div class="search__header">
          <div class="row">
            <label for="TipoCarro" class="tabs__label">
              <input checked="" class="tabs__input" type="radio" name="TipoVeiculo" id="TipoCarro" value="carro">
              <span class="tabs__item">
                <svg class="tabs__icon tabs__icon-car" version="1.0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 38.46 12.25">
                  <path d="M37.71,5.9l.46-.8a.49.49,0,0,0-.07-.58c-.11-.11-.43-.46-3.76-.83a.48.48,0,0,0-.53.43.49.49,0,0,0,.43.54,27.5,27.5,0,0,1,2.81.44l-.44.78a.49.49,0,0,0,.22.69l.61.28c-.22,1.07-.66,2.14-1.06,2.23A16.5,16.5,0,0,1,34,9.33a3.23,3.23,0,1,0-6.18.06L9.4,9.82a3.3,3.3,0,0,0,.11-.92A3.23,3.23,0,1,0,3,9.06a2.67,2.67,0,0,0,.17.91C1.52,9.86,1.49,8.68,1,6.87a2,2,0,0,0,.72-1,2.16,2.16,0,0,0-.08-1.39,5.28,5.28,0,0,0,2.67-.8c.11,0,.47-.22,1.45-.7,1.14-.57,3.06-1.52,3.43-1.63a16.17,16.17,0,0,1,1.7-.15A20.17,20.17,0,0,0,9.27,2.55a.49.49,0,0,0,.26.79c.12,0,2.86.64,3.55.75.15,0,1,0,2.21,0,3.19-.07,8.84-.28,10.33-.33l3.19.2a.46.46,0,0,0,.28-.07,4.63,4.63,0,0,1,2.3-.66c1.3.06,3.19.26,3.21.26A.48.48,0,0,0,35.14,3a.49.49,0,0,0-.43-.54c-.08,0-1.94-.2-3.27-.26A5.44,5.44,0,0,0,28.72,3l-3-.18c-.77-.43-4.52-2.5-6-2.7A84.15,84.15,0,0,0,8.93.4c-.41.13-1.86.84-3.57,1.69L4,2.74a10.63,10.63,0,0,1-3.16.85.48.48,0,0,0-.37.28.49.49,0,0,0,0,.46,1.58,1.58,0,0,1,.3,1.23,1.15,1.15,0,0,1-.55.64A.49.49,0,0,0,0,6.76c.43,1.95,1.18,3.3,2,3.71a3.34,3.34,0,0,0,1.76.62A3.21,3.21,0,0,0,9,10.81l19.39-.45a3.2,3.2,0,0,0,5.19,0,20.11,20.11,0,0,0,3-.29c1.34-.3,1.8-2.88,1.88-3.4a.49.49,0,0,0-.28-.52ZM14.54,1a47.93,47.93,0,0,1,5.07,0,19.48,19.48,0,0,1,4.21,1.79c-3.65.13-8,.27-9.77.3Zm-1,0-.47,2c-.52-.09-1.58-.32-2.37-.5.51-.41,1.23-1,1.92-1.5l.92,0ZM6.34,11.27A2.29,2.29,0,1,1,8.55,8.93a2.28,2.28,0,0,1-2.21,2.34ZM31,10.69a2.29,2.29,0,1,1,2.21-2.34A2.28,2.28,0,0,1,31,10.69Z"/>
                </svg>
                <span class="tabs__text">
                  Comprar
                  <span class="tabs__subtitle">Carros</span>
                </span>
              </span>
            </label>

            <label for="TipoMoto" class="tabs__label">
              <input class="tabs__input" type="radio" name="TipoVeiculo" id="TipoMoto" value="moto">
              <span class="tabs__item">
                <svg class="tabs__icon tabs__icon-bike" version="1.0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.67 15">
                  <path d="M15.44,0H12.76V1.22h2l.65,1.2H12.7A2.46,2.46,0,0,0,11.09,3L9.47,4.39H1.61V5.61H4a5.26,5.26,0,0,1,5.2,4.69H8A4.05,4.05,0,0,0,4,6.83,4.09,4.09,0,0,0,1.78,14.3,4,4,0,0,0,4,15a4.05,4.05,0,0,0,4-3.47h5.52l3.71-5.61.94,1.75a4,4,0,1,0,2.44-.83,4,4,0,0,0-1.38.25ZM9.73,7.87A6.53,6.53,0,0,0,7.78,5.61H9.91l2-1.66a1.28,1.28,0,0,1,.83-.31H16l.57,1.06L14.52,7.87Zm10.38,3.34,1.06-.58L19.85,8.17a2.79,2.79,0,0,1,.8-.12,2.85,2.85,0,1,1-1.85.7ZM4,13.78A2.87,2.87,0,1,1,6.8,10.31H4v1.22H6.8A2.84,2.84,0,0,1,4,13.78Zm9.68-4.69-.81,1.22H10.45a6.61,6.61,0,0,0-.23-1.22Z"/>
                </svg>
                <span class="tabs__text">
                  Comprar
                  <span class="tabs__subtitle">Motos</span>
                </span>
              </span>
            </label>
          </div>
          <div class="search__sale">
            <a href="#" class="search__sale-button">Vender meu carro</a>
          </div>
        </div>
        <div class="search__content">
          <div class="row">
            <label class="checkbox" for="ConservacaoNovos">
              <input checked="" class="checkbox__input" type="checkbox" name="ConservacaoVeiculo" id="ConservacaoNovos">
              <span class="checkbox__label">Novos</span>
            </label>
            <label class="checkbox" for="ConservacaoUsados">
              <input checked="" class="checkbox__input" type="checkbox" name="ConservacaoVeiculo" id="ConservacaoUsados">
              <span class="checkbox__label">Usados</span>
            </label>
          </div>
          <div class="row">
            <div class="half-column">
              <div class="input">
                <label for="Onde" class="input__pretext input__pretext-pin">Onde</label>
                <input type="text" name="Onde" id="Onde">
                <label class="input__postext">
                  <label for="Raio" class="input__pretext">Raio</label>
                  <select name="Raio" id="Raio">
                    <option value="">Raio</option>
                    <?php $item = array(25,50,100,150,200,250);
                    foreach ($item as $key => $value) { ?>
                      <option value="<?php echo $value ?>"><?php echo $value ?>km</option>
                    <?php } ?>
                  </select>
                </label>
              </div>
            </div>
            <div class="half-column row">
              <div class="half-column">
                <div class="input">
                  <label for="Marcas" class="input__pretext">Marcas</label>
                  <select name="Marcas" id="Marcas">
                    <option value="Todas">Todas</option>
                  </select>
                </div>
              </div>
              <div class="half-column">
                <div class="input">
                  <label for="Modelos" class="input__pretext">Modelos</label>
                  <select name="Modelos" id="Modelos">
                    <option value="Todas">Todos</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row ano-preco-versao">
            <div class="half-column row">
              <div class="half-column">
                <div class="input">
                  <label for="Ano" class="input__pretext">Ano Desejado</label>
                  <input data-input-option type="text" name="Ano" id="Ano">
                  <div class="input__boxrange">
                    <div class="input">
                      <label for="AnoDe" class="input__pretext">De</label>
                      <select name="AnoDe" id="AnoDe">
                        <option value="">Min</option>
                        <?php for($i=date('Y');$i>1881;$i--){ ?>
                          <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="input">
                      <label for="AnoAte" class="input__pretext">Até</label>
                      <select name="AnoAte" id="AnoAte">
                        <option value="">Máx</option>
                        <?php for($i=date('Y');$i>1881;$i--){ ?>
                          <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="half-column">
                <div class="input">
                  <label for="FaixaPreco" class="input__pretext">Faixa de Preço</label>
                  <input data-input-option type="text" name="FaixaPreco" id="FaixaPreco">
                  <div class="input__boxrange">
                    <div class="input">
                      <label for="PrecoDe" class="input__pretext">De</label>
                      <input class="input__price" name="PrecoDe" id="PrecoDe" type="number">
                    </div>
                    <div class="input">
                      <label for="PrecoAte" class="input__pretext">Até</label>
                      <input class="input__price" name="PrecoAte" id="PrecoAte" type="number">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="half-column">
              <div class="input">
                <label for="Versao" class="input__pretext">Versão</label>
                <select name="Versao" id="Versao">
                  <option value="Todas">Todas</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row search__buttons">
            <a href="#" class="search__advanced">Busca Avançadas</a>
            <div class="search__filter">
              <button type="reset" class="search__filter-clear">Limpar filtros</button>
              <button type="submit" class="search__filter-submit">Ver ofertas</button>
            </div>
          </div>
        </div>
      </form>
    </main>
  </div>
</body>
</html>